package com.async.Spring_Async.controller;

import com.async.Spring_Async.model.User;
import com.async.Spring_Async.service.GitHubLookupService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.CancellationException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@RestController
public class AsyncController {
    private static final Logger log= LoggerFactory.getLogger(AsyncController.class);

    @Autowired
    GitHubLookupService gitHubLookupService;

    @RequestMapping("/Async")
    public String aysnc() throws InterruptedException, CancellationException, ExecutionException {
        long start=System.currentTimeMillis();
        //Kick off multiple async lookups
        CompletableFuture<User> user1=gitHubLookupService.findUser("PivotalSoftware");
        CompletableFuture<User> user2=gitHubLookupService.findUser("CloudFoundry");
        CompletableFuture<User> user3=gitHubLookupService.findUser("Spring-Projects");

        log.info("Elapsed Time"+(System.currentTimeMillis()-start));
        log.info("--> " + user1.get());
        log.info("--> " + user2.get());
        log.info("--> " + user3.get());
        return "Method was completed Successfully";
    }
}
